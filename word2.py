#!usr/bin/env python

import re
import string

frequency = {}

document_text = open('/home/ec2-user/file.txt', 'r') 
text_string = document_text.read().lower()
match_pattern = re.findall(r'\b[a-z]{1,15}\b', text_string)

for word in match_pattern:
    count = frequency.get(word,0)
    frequency[word] = count + 1

#create a list for all the words#
     
frequency_list = frequency.keys()
 
for words in frequency_list:
    print frequency[words],words 